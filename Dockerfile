FROM docker.io/gradle:7.5.1-jdk17-alpine as BASE

WORKDIR /usr/app/source
COPY . /usr/app/source/

RUN gradle build

FROM docker.io/eclipse-temurin:17.0.5_8-jre-alpine

COPY --from=BASE /usr/app/source/build/libs/lab1_microservice-0.0.1-SNAPSHOT.jarx /app/app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar","/app/app.jar"]