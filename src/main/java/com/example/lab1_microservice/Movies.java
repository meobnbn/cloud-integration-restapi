package com.example.lab1_microservice;
import javax.persistence.*;
import java.util.List;

@Entity

public class Movies {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String title;
    private String author;
    private String description;
    @Transient
    private List<Actors> actors;

    public Movies(String _title, String _author, String _description){
        this.title = _title;
        this.author = _author;
        this.description = _description;
    }

    public Movies(String _title, String _author, String _description, List<Actors> _actors) {
        this.title = _title;
        this.author = _author;
        this.description = _description;
        this.actors = _actors;
    }



    public Movies() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Actors> getActors() {
        return actors;
    }

    public void setActors(List<Actors> actors) {
        this.actors = actors;
    }
}
