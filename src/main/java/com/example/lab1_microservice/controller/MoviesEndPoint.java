package com.example.lab1_microservice.controller;

import com.example.lab1_microservice.Actors;
import com.example.lab1_microservice.Movies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.example.lab1_microservice.repository.MoviesRepository;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;
import java.util.Optional;


@RestController
@RequestMapping(path="/api/movies")
public class MoviesEndPoint {

    @Autowired
    private MoviesRepository moviesRepository;

    @GetMapping(path="")
    public @ResponseBody Iterable<Movies> findAllMovies() {
        return moviesRepository.findAll();
    }

    private final CircuitBreakerFactory circuitBreakerFactory;

    public MoviesEndPoint(MoviesRepository moviesRepository, CircuitBreakerFactory circuitBreakerFactory) {
        this.moviesRepository = moviesRepository;
        this.circuitBreakerFactory = circuitBreakerFactory;
    }

    // private final String URL_ACTORS = "http://microservice_lab2:8081/actors";
    private final String URL_ACTORS = "http://Lab2-env.eba-mrvespwc.us-east-1.elasticbeanstalk.com/actors";

    @GetMapping("/{id}")
    public ResponseEntity<Movies> findAMovie(@PathVariable int id) {
        Optional<Movies> movie = moviesRepository.findById(id);

        if(movie.isPresent()) {
            RestTemplate restTemplate = new RestTemplate();
            List<Actors> actorsList = circuitBreakerFactory.create("findAMovie").run(
                    () -> List.of(
                            Objects.requireNonNull(
                                    restTemplate.getForObject(URL_ACTORS + "/" + id, Actors[].class)
                            )
                    ), throwable -> List.of()
            );

            return ResponseEntity.ok().body(new Movies(movie.get().getTitle(), movie.get().getAuthor(), movie.get().getDescription(), actorsList)
            );
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/add")
    public Movies createMovie(@Validated @RequestBody Movies movie) {
        return moviesRepository.save(movie);
    }

    @PutMapping("/update/{id}")
    public Movies updateMovie(@RequestBody Movies newMovie, @PathVariable int id) {

        Movies updatedMovie = moviesRepository.findById(id)
                .map(movies -> {
                    movies.setAuthor(newMovie.getAuthor());
                    movies.setTitle(newMovie.getTitle());
                    movies.setDescription(newMovie.getDescription());
                    return moviesRepository.save(movies);
                })
                .orElseGet(() -> {
                    newMovie.setId(id);
                    return moviesRepository.save(newMovie);
                });

        return updatedMovie;
    }

    @DeleteMapping("/delete/{id}")
    public Optional<Movies> deleteMovie(@PathVariable int id){
        Optional<Movies> deletedMovie = moviesRepository.findById(id);
        moviesRepository.deleteById(id);
        return deletedMovie;
    }

    @DeleteMapping("/delete")
    public void deleteAllMovies(){
        moviesRepository.deleteAll();
    }
}
