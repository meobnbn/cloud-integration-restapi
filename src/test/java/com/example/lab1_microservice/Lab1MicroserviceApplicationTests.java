// package com.example.lab1_microservice;

// import com.example.lab1_microservice.repository.MoviesRepository;
// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.Test;
// import org.junit.runner.RunWith;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.http.MediaType;
// import org.springframework.test.context.TestPropertySource;
// import org.springframework.test.context.junit4.SpringRunner;
// import org.springframework.test.web.servlet.MockMvc;
// import org.springframework.test.web.servlet.ResultMatcher;
// import org.springframework.test.web.servlet.setup.MockMvcBuilders;
// import org.springframework.web.context.WebApplicationContext;


// import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
// import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
// import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

// @SpringBootTest
// @AutoConfigureMockMvc
// class Lab1MicroserviceApplicationTests {

//     private MockMvc mvc;

//     @Autowired
//     private MoviesRepository moviesRepository;
//     @Autowired
//     private WebApplicationContext applicationContext;

//     final Movies movie1 = new Movies("Film 1", "Meo", "lalalala");
//     final Movies movie2 = new Movies("Film 2", "Meo 2", "lolololo");

//     @BeforeEach
//     void setup() {
//         //moviesRepository.deleteAll();
//         //createTestMovies();
//         this.mvc = MockMvcBuilders
//                 .webAppContextSetup(applicationContext)
//                 .build();
//     }

//     @Test
//     public void givenMovies_whenGetMovies_thenStatus200()
//             throws Exception {

//         mvc.perform(get("/api/movies")
//                         .contentType(MediaType.APPLICATION_JSON))
//                 .andExpect(status().isOk())
//                 .andExpect(content()
//                         .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                 .andExpect(jsonPath("$").isArray())
//                 .andExpect(jsonPath("$", hasSize(5)));
//     }

//     @Test
//     public void givenAMovie_whenGetAMovie_thenStatus200()
//             throws Exception {
//         Movies movie = moviesRepository.save(movie1);
//         mvc.perform(get("/api/movies/19")
//                         .contentType(MediaType.APPLICATION_JSON))
//                 .andExpect(status().isOk())
//                 .andExpect(content()
//                         .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                 .andExpect(jsonPath("$.title").value("Film 1"))
//                 .andExpect(jsonPath("$.author").value("Meo"))
//                 .andExpect(jsonPath("$.description").value("lalalala"));
//     }

//     @Test
//     public void noMovie_afterDeletingAllMovies_thenStatus200()
//             throws Exception {
//         mvc.perform(delete("/api/movies/delete")
//                         .contentType(MediaType.APPLICATION_JSON))
//                 .andExpect(status().isOk())
//                 .andExpect(content()
//                         .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                 .andExpect((ResultMatcher) jsonPath("$").doesNotExist());
//     }

//     @Test
//     public void createMovie_checkAndReturn_a_movie() throws Exception {
//         mvc.perform(post("/api/movies/add")
//                         .content(
//                                 "{\"title\": \"Film test\"," +
//                                         "\"author\": \"bibi\"," +
//                                         "\"description\": \"nananana\"" + "}"
//                         )
//                         .contentType(MediaType.APPLICATION_JSON)
//                         .accept(MediaType.APPLICATION_JSON))
//                 .andExpect(status().isOk())
//                 .andExpect(jsonPath("$.title").value("Film test"))
//                 .andExpect(jsonPath("$.author").value("bibi"))
//                 .andExpect(jsonPath("$.description").value("nananana"));
//     }

//     @Test
//     public void updateMovie_checkAndReturn_a_movie() throws Exception {
//         Movies movie = moviesRepository.save(movie1);

//         mvc.perform(put("/api/movies/update/" + movie.getId())
//                         .content(
//                                 "{\"title\": \"Avatar\"," +
//                                         "\"author\": \"Moi\"," +
//                                         "\"description\": \"lalelezrlzae\" + }"
//                         )
//                         .contentType(MediaType.APPLICATION_JSON)
//                         .accept(MediaType.APPLICATION_JSON))
//                 .andExpect(status().isOk())
//                 .andExpect(jsonPath("$.title").value("Avatar"))
//                 .andExpect(jsonPath("$.author").value("Moi"))
//                 .andExpect(jsonPath("$.description").value("lalelezrlzae"));
//     }


//     @Test
//     void contextLoads() {
//     }

// }
